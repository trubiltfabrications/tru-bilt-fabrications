Steel sheds Melbourne is our business. We have been in the game since 1966 and are truly experienced in providing quality steel structures that are built to last. Each of our sheds are individually engineered so you know that you are purchasing a shed of the highest quality and standard.

Address: 42-46 Tarnard Drive, Braeside, VIC 3195, Australia

Phone: +61 3 9580 0199
